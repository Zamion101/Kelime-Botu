package me.Zamion101.KelimeOyunu;

import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BotMemory {

    private static Map<Double,List<String>> words = new HashMap<Double, List<String>>();
    private static Map<Double,IUser> lastUser = new HashMap<Double, IUser>();
    private static Map<IGuild,Boolean> gameStarted = new HashMap<>();


    public static void startGame(IGuild guild){
        if(gameStarted.containsKey(guild)){
            gameStarted.remove(guild);
            gameStarted.put(guild,true);
            return;
        }
        gameStarted.put(guild,true);
    }

    public static void stopGame(IGuild guild){
        gameStarted.remove(guild);
        gameStarted.put(guild,false);
        clearStage(guild.getLongID());
        BotUtils.removeGuild(guild);
    }

    public static boolean isGameStarted(IGuild guild){
        if(!gameStarted.containsKey(guild)){
            return false;
        }
        return gameStarted.get(guild);
    }


    public static boolean addWord(double server,String word){
        if(!words.containsKey(server)){
            words.put(server,new ArrayList<String>());
        }
        List<String> twords = words.get(server);
        if(twords.contains(word)){
            return false;
        }else{
            twords.add(word);
            words.remove(server);
            words.put(server,twords);
            return true;
        }
    }

    public static IUser getLastUser(double server){
        if(!lastUser.containsKey(server)){
            lastUser.put(server,KelimeBot.cl.getOurUser());
        }
        return lastUser.get(server);
    }

    public static void setLastUser(double server,IUser last){
        lastUser.remove(server);
        lastUser.put(server,last);
    }

    public static String getLastWordEndChar(double server){
        if(!words.containsKey(server)){
            words.put(server,new ArrayList<>());
        }
        List<String> temp = words.get(server);
        String cha = "";
        try{
            if(temp.get(temp.size()-1) != null){
                cha = temp.get(temp.size()-1);
            }
        }catch (ArrayIndexOutOfBoundsException disabled){}

        return cha.substring(cha.length()-1,cha.length());
    }

    public static void clearStage(double server){
            words.remove(server);
            words.put(server,new ArrayList<String>());
            lastUser.remove(server);
            lastUser.put(server,KelimeBot.cl.getOurUser());
    }

    static List<String> bitirme = new ArrayList<>();

    public static void initBitirme(){
        addBitirme("UĞURLUDAĞ");
        addBitirme("ULUDAĞ");
        addBitirme("ALTINDAĞ");
        addBitirme("SAMANDAĞ");
        addBitirme("TEBELLÜĞ");
        addBitirme("TEKİRDAĞ");
        addBitirme("YANARDAĞ");
        addBitirme("AKÇADAĞ");
        addBitirme("BABADAĞ");
        addBitirme("ELMADAĞ");
        addBitirme("EMİRDAĞ");
        addBitirme("SADEYAĞ");
        addBitirme("SARIFİĞ");
        addBitirme("SIRADAĞ");
        addBitirme("ALADAĞ");
        addBitirme("BAŞBUĞ");
        addBitirme("BEYDAĞ");
        addBitirme("DEBBAĞ");
        addBitirme("ELAZIĞ");
        addBitirme("MEBLAĞ");
        addBitirme("SAĞYAĞ");
        addBitirme("TEBLİĞ");
        addBitirme("BALİĞ");
        addBitirme("BELİĞ");
        addBitirme("BÜLUĞ");
        addBitirme("ÇIRAĞ");
        addBitirme("DİMAĞ");
        addBitirme("DİRİĞ");
        addBitirme("FARİĞ");
        addBitirme("FERAĞ");
        addBitirme("İBLAĞ");
        addBitirme("İFRAĞ");
        addBitirme("OTAĞ");
        addBitirme("BAĞ");
        addBitirme("BÖĞ");
        addBitirme("CAĞ");
        addBitirme("ÇAĞ");
        addBitirme("ÇIĞ");
        addBitirme("ÇİĞ");
        addBitirme("DAĞ");
        addBitirme("FİĞ");
        addBitirme("HUĞ");
        addBitirme("KIĞ");
        addBitirme("LIĞ");
        addBitirme("LOĞ");
        addBitirme("SAĞ");
        addBitirme("SIĞ");
        addBitirme("TIĞ");
        addBitirme("TİĞ");
        addBitirme("TUĞ");
        addBitirme("YAĞ");
        addBitirme("YEĞ");
        addBitirme("YOĞ");
        addBitirme("ZAĞ");
        addBitirme("AĞ");
        addBitirme("İĞ");
    }

    public static List<String> getBitirme() {
        return bitirme;
    }

    private static void addBitirme(String word){
        bitirme.add(word);
    }

    private static Map<IGuild,Integer> kelimesay = new HashMap<>();

    public static int getKelimeSayısı(IGuild guild){
        if(!kelimesay.containsKey(guild)){
            return 0;
        }
        return kelimesay.get(guild);
    }

    public static void resetKelimeSayısı(IGuild guild){
        if(!kelimesay.containsKey(guild))return;
        kelimesay.remove(guild);
        kelimesay.put(guild,0);
    }

    public static void addKelimeSayısı(IGuild guild){
        if(!kelimesay.containsKey(guild)){
            kelimesay.put(guild,1);
            return;
        }
        int number = kelimesay.get(guild) + 1;
        kelimesay.remove(guild);
        kelimesay.put(guild,number);
    }

    public static boolean isFinishable(IGuild guild){
        if(!kelimesay.containsKey(guild)){
            return false;
        }
        return kelimesay.get(guild) >= MysqlData.getWordLimit(guild);
    }

    private static List<String> blacklist = new ArrayList<>();

    public static boolean isBlacklisted(IGuild guild){
        return blacklist.contains(guild.getStringID());
    }
    public static boolean isBlacklisted(String guildID){
        return blacklist.contains(guildID);
    }

    public static void addGuildToBlacklist(String guildID){
        if(!blacklist.contains(guildID))blacklist.add(guildID);
    }

    public static void resetBlacklisted(){
        blacklist.clear();
        blacklist = MysqlData.getBlacklist();
    }
}
