package me.Zamion101.KelimeOyunu.listener;

import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;
import sx.blah.discord.handle.obj.IGuild;


public class OnJoinGuild implements IListener<GuildCreateEvent> {

    @Override
    public void handle(GuildCreateEvent guildCreateEvent) {
        IGuild guild = guildCreateEvent.getGuild();
        BotUtils.addGuild(guild, null);
    }


}
