package me.Zamion101.KelimeOyunu.listener;

import me.Zamion101.KelimeOyunu.*;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.CommandAPI;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import me.Zamion101.KelimeOyunu.experimentals.utils.GuildMemory;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

import java.util.Locale;

public class MessageListener implements IListener<MessageReceivedEvent> {

    static String[] alphabet = new String[]{"a","b","c","ç","d","e","f","g","ğ","h","ı","i","j","k","l","m","n","o","ö","p","r","s","ş","t","u","ü","v","y","z"};

    public void handle(MessageReceivedEvent messageReceivedEvent) {
        if(KelimeBot.cl.isReady()){
            IGuild guild = messageReceivedEvent.getGuild();
            IMessage msg = messageReceivedEvent.getMessage();
            String message = messageReceivedEvent.getMessage().getContent().toLowerCase(new Locale("tr","TR"));
            IUser author = messageReceivedEvent.getAuthor();
            double server = messageReceivedEvent.getGuild().getLongID();
            IChannel channel = messageReceivedEvent.getChannel();

            String prefix = MysqlData.getPrefix(guild);

            if(!author.isBot()){
                if(message.startsWith(prefix)){
                    message = message.replaceAll(prefix,"");
                    String[] raw = message.split(" ") != null ? message.split(" ") : new String[]{message};
                    String cmd = raw[0];

                    String[] args = new String[raw.length];
                    if(raw.length > 1){
                        for(int a = 0; a < raw.length-1;a++){
                            args[a] = raw[a+1];
                        }
                    }else{
                        args[0] = null;
                    }
                    if(CommandAPI.isCommandExist(cmd)){
                        msg.delete();
                        CommandAPI.runCommand(cmd,args,messageReceivedEvent);
                        return;
                    }else{
                        msg.delete();
                        BotUtils.sendMessage(channel,true, Vars.formatMessage(Vars.UNKNOWN_COMMAND,author.mention(),MysqlData.getPrefix(guild)));
                        return;
                    }
                }
                if(MysqlData.getChannel(guild) == null){
                    return;
                }
                if(MysqlData.getChannel(guild).equals(channel)) {
                    if(!BotMemory.isGameStarted(messageReceivedEvent.getGuild())){
                        return;
                    }
                    if (!message.startsWith("(")) {
                        if (BotMemory.getLastUser(server).getLongID() == author.getLongID()) {
                            msg.delete();
                            BotUtils.sendMessage(channel, true, Vars.formatMessage(Vars.ALREADY_QUEUE,author.mention()));
                        } else {
                            if(message.toLowerCase(new Locale("tr","TR")).length() <= 20){
                                if(message.toLowerCase(new Locale("tr","TR")).length() > 1){
                                    boolean yasak = false;
                                    String stryasak = message;
                                    for(String s : alphabet){
                                       stryasak = stryasak.replace(s,"");
                                    }
                                    if(stryasak.length() > 0){
                                        yasak = true;
                                        if(stryasak.contains(" ")){
                                            stryasak = "`BOŞLUK`";
                                        }else{
                                            stryasak = "`" + stryasak.substring(0,1) + "`";
                                        }
                                    }
                                    String lastChar = BotMemory.getLastWordEndChar(server);
                                    if(!yasak){
                                        if (message.startsWith(lastChar)) {
                                            if (message.endsWith("ğ")) {
                                                boolean bitirdi = false;
                                                for(String bit : BotMemory.getBitirme()){
                                                    if(message.toLowerCase(new Locale("tr","TR")).equals(bit.toLowerCase())){
                                                        if(!BotMemory.isFinishable(guild)){
                                                            msg.delete();
                                                            BotUtils.sendMessage(channel,true,Vars.formatMessage(Vars.WORD_LIMIT,author.mention(),MysqlData.getWordLimit(guild) - BotMemory.getKelimeSayısı(guild)));
                                                            return;
                                                        }
                                                        bitirdi = true;
                                                        break;
                                                    }
                                                }
                                                if(bitirdi){
                                                    BotMemory.resetKelimeSayısı(guild);
                                                    BotMemory.clearStage(server);
                                                    MysqlData.addUserToBoard(guild,author);
                                                    if(GuildMemory.getGuild(guild).isOto_start()){
                                                        String rand = RandomStringGenerator.generateRandomString(1);
                                                        BotMemory.addWord(server, rand);
                                                        KelimeBot.sendEndEmbed(channel,author,message,0);
                                                        new java.util.Timer().schedule(
                                                                new java.util.TimerTask() {
                                                                    @Override
                                                                    public void run() {
                                                                        KelimeBot.sendStartEmbed(channel,rand);
                                                                    }
                                                                },
                                                                3000
                                                        );
                                                        return;
                                                    }
                                                    BotMemory.stopGame(guild);
                                                    KelimeBot.sendEndEmbed(channel,author,message,1);
                                                }else{
                                                    msg.delete();
                                                    BotUtils.sendMessage(channel, true, Vars.formatMessage(Vars.MEANLESS_WORD,author.mention()));

                                                }
                                            } else {
                                                if (!BotMemory.addWord(server, message)) {
                                                    msg.delete();
                                                    BotUtils.sendMessage(channel, true, Vars.formatMessage(Vars.WORD_ALREADY_USED,author.mention()));
                                                } else {
                                                    BotMemory.addKelimeSayısı(guild);
                                                    BotMemory.setLastUser(server, author);
                                                }
                                            }
                                        } else {
                                            msg.delete();
                                            BotUtils.sendMessage(channel, true, Vars.formatMessage(Vars.WORD_MUST_START_WITH,author.mention(),lastChar));
                                        }
                                    }else{
                                        msg.delete();
                                        BotUtils.sendMessage(channel, true, Vars.formatMessage(Vars.WORD_DONT_CONTAINS,author.mention(),stryasak));
                                    }
                                }else{
                                    msg.delete();
                                    BotUtils.sendMessage(channel, true, Vars.formatMessage(Vars.WORD_LENGTH_TWO,author.mention()));
                                }
                            }else {
                                msg.delete();
                                BotUtils.sendMessage(channel, true, Vars.formatMessage(Vars.WORD_LENGTH_TWENTY,author.mention()));
                            }
                        }

                    }else{
                        if(!message.endsWith(")")){
                            msg.delete();
                            BotUtils.sendMessage(channel, true, Vars.formatMessage(Vars.GAME_MESSAGE_FORMAT_MUST_BE,author.mention()));
                        }
                    }
                }
            }


        }
        }
}
