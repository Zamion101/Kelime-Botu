package me.Zamion101.KelimeOyunu.listener;

import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.shard.ReconnectSuccessEvent;
import sx.blah.discord.handle.obj.IGuild;

import java.util.ArrayList;
import java.util.List;

public class DisconnectEvent implements IListener<ReconnectSuccessEvent> {

    public static List<IGuild> registeredGuilds = new ArrayList<>();

    @Override
    public void handle(ReconnectSuccessEvent disconnectedEvent) {
        registeredGuilds = disconnectedEvent.getClient().getGuilds();
    }

    public static boolean isGuildRegistered(IGuild guild){
        return registeredGuilds.contains(guild);
    }
}
