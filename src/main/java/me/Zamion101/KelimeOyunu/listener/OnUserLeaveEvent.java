package me.Zamion101.KelimeOyunu.listener;

import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.member.UserLeaveEvent;

public class OnUserLeaveEvent implements IListener<UserLeaveEvent> {

    @Override
    public void handle(UserLeaveEvent userLeaveEvent) {
        MysqlData.removeUser(userLeaveEvent.getGuild(),userLeaveEvent.getUser());
    }
}
