package me.Zamion101.KelimeOyunu.listener;

import me.Zamion101.KelimeOyunu.BotUtils;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.GuildLeaveEvent;
import sx.blah.discord.handle.obj.IGuild;

public class OnGuildLeaveEvent implements IListener<GuildLeaveEvent> {

    @Override
    public void handle(GuildLeaveEvent guildLeaveEvent) {

        IGuild guild = guildLeaveEvent.getGuild();
        BotUtils.removeGuild(guild);
        BotUtils.deleteGuild(guild);
    }
}
