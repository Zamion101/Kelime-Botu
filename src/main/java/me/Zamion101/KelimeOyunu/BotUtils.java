package me.Zamion101.KelimeOyunu;

import javafx.application.Platform;
import me.Zamion101.KelimeOyunu.GUI.BotGUI;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import me.Zamion101.KelimeOyunu.listener.DisconnectEvent;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BotUtils {

    private static int totalUser = 0;

    public static void sendMessage(IChannel channel,boolean del, String message) {
        RequestBuffer.request(() -> {
            try {
                try {
                    IMessage msg = channel.sendMessage(message);
                    if (del) {
                        new java.util.Timer().schedule(
                                new java.util.TimerTask() {
                                    @Override
                                    public void run() {
                                        msg.delete();
                                    }
                                },
                                3000
                        );
                    }
                } catch (MissingPermissionsException ex) {
                    if(BotMemory.isBlacklisted(channel.getGuild())) return;
                    channel.getGuild().getOwner().getOrCreatePMChannel().sendMessage("Merhaba " +channel.getGuild().getOwner().mention() +"!\nSahibi olduğun `" + channel.getGuild().getName() + "` adlı sunucuna Kelime Oyunlarını yönetmek için" +
                            " geldim fakat bana gerekli izinleri vermediğin sürece bu işimi yerine getiremem.\n" +
                            "Bana Gerekli olan yetkiler;\n" +
                            "`Kanalları Yönetme (MANAGE_CHANNELS)\n" +
                            "Mesajları Yönetme (MANAGE_MESSAGES)\n" +
                            "Mesaj Gönderme (SEND_MESSAGES)\n" +
                            "Kanalları Görme (VIEW_CHANNEL)\n" +
                            "Sunucuyu Yönetme (MANAGE_CHANNEL) [Sunucu ayarlarını hiçbir şekilde ellemiyorum sadece istatistik toplamak için lazım.]\n" +
                            "Herkesi Etiketleme (MENTION_EVERYONE)`\n" +
                            "Gerekli yetkileri verdiysen `" + MysqlData.getPrefix(channel.getGuild()) +"setup` yaz ki kuruluma başlayabileyim.");
                    /**channel.getGuild().getOwner().getOrCreatePMChannel().sendMessage("I don't have enough permission to run perfectly. This perrmissions what i need to work perfectly;\n" +
                            "`Manage Channels (MANAGE_CHANNELS)\n" +
                            "Manage Messages (MANAGE_MESSAGES)\n" +
                            "Sending Messages (SEND_MESSAGES)\n" +
                            "View Messages (VIEW_CHANNEL)\n" +
                            "Mention About User (MENTION_EVERYONE)`\n" +
                            "If already set the permissions please use `ko!setup` command to setup game.");*/
                }catch (DiscordException disabled){
                }

            } catch (DiscordException e) {
                System.err.println("Message could not be sent with error: ");
                e.printStackTrace();
            }
        });
    }

    public static void sendMessage(IChannel channel,boolean del,long time, String message){
        time = time * 1000;
        // This might look weird but it'll be explained in another page.
        long finalTime = time;
        RequestBuffer.request(() -> {
            try{
                try{
                    IMessage msg = channel.sendMessage(message);
                    if(del){
                        new java.util.Timer().schedule(
                                new java.util.TimerTask() {
                                    @Override
                                    public void run() {
                                        msg.delete();
                                    }
                                },
                                finalTime
                        );
                    }
                }catch (MissingPermissionsException ex){
                    if(BotMemory.isBlacklisted(channel.getGuild())) return;
                    channel.getGuild().getOwner().getOrCreatePMChannel().sendMessage("Merhaba " +channel.getGuild().getOwner().mention() +"!\nSahibi olduğun `" + channel.getGuild().getName() + "` adlı sunucuna Kelime Oyunlarını yönetmek için" +
                            " geldim fakat bana gerekli izinleri vermediğin sürece bu işimi yerine getiremem.\n" +
                            "Bana Gerekli olan yetkiler;\n" +
                            "`Kanalları Yönetme (MANAGE_CHANNELS)\n" +
                            "Mesajları Yönetme (MANAGE_MESSAGES)\n" +
                            "Mesaj Gönderme (SEND_MESSAGES)\n" +
                            "Kanalları Görme (VIEW_CHANNEL)\n" +
                            "Sunucuyu Yönetme (MANAGE_CHANNEL) [Sunucu ayarlarını hiçbir şekilde ellemiyorum sadece istatistik toplamak için lazım.]\n" +
                            "Herkesi Etiketleme (MENTION_EVERYONE)`\n" +
                            "Gerekli yetkileri verdiysen `" + MysqlData.getPrefix(channel.getGuild()) + "setup` yaz ki kuruluma başlayabileyim.");
                    /**channel.getGuild().getOwner().getOrCreatePMChannel().sendMessage("I don't have enough permission to run perfectly. This perrmissions what i need to work perfectly;\n" +
                            "`Manage Channels (MANAGE_CHANNELS)\n" +
                            "Manage Messages (MANAGE_MESSAGES)\n" +
                            "Sending Messages (SEND_MESSAGES)\n" +
                            "View Messages (VIEW_CHANNEL)\n" +
                            "Mention About User (MENTION_EVERYONE)`\n" +
                            "If already set the permissions please use `ko!setup` command to setup game.");*/
                }

            } catch (DiscordException e){
                System.err.println("Message could not be sent with error: ");
                e.printStackTrace();
            }
        });

        /*
        // The below example is written to demonstrate sending a message if you want to catch the RLE for logging purposes
        RequestBuffer.request(() -> {
            try{
                channel.sendMessage(message);
            } catch (RateLimitException e){
                System.out.println("Do some logging");
                throw e;
            }
        });
        */

    }

    private static List<IGuild> guilds = new ArrayList<>();
    private static Map<IGuild,IChannel> channels = new HashMap<>();

    public static void removeGuild(IGuild guild){
        guilds.remove(guild);
        channels.remove(guild);
        totalUser -= guild.getUsers().size();
        KelimeBot.cl.changePresence(StatusType.ONLINE, ActivityType.PLAYING,getTotalUser() + " Kişi ile Kelime Oyunu ");
    }
    public static void deleteGuild(IGuild guild){
        MysqlData.removeGuild(guild);
    }

    public static int getGuildSize(){
        return guilds.size();
    }


    public static void addGuild(IGuild guild,String channelName){
        if(DisconnectEvent.isGuildRegistered(guild)){
            return;
        }
        if(!KelimeBot.cl.isReady()){
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    addGuild(guild,channelName);
                                }
                            });
                        }
                    },
                    3000
            );
            return;
        }
        if(channelName != null){
            if(MysqlData.getChannel(guild) != null){
                IChannel delChannel = MysqlData.getChannel(guild);
                delChannel.delete();
            }
            IChannel channel = guild.createChannel(channelName);
            MysqlData.addGuild(guild,channel);
            MysqlData.initSettings(guild);
            MysqlData.setChannel(guild,channel);
            channels.put(guild,channel);
            BotGUI.addGuild(guild);
            tryStartGame(guild,channel);

            return;
        }
        if(!guilds.contains(guild)){
            guilds.add(guild);
            if(MysqlData.getChannel(guild) != null){
                IChannel channel = MysqlData.getChannel(guild);
                channels.put(guild,channel);
                BotGUI.addGuild(guild);
                tryStartGame(guild,channel);
            }else{
                try {
                    IChannel channel;
                    if(channelName != null){
                        channel = guild.createChannel(channelName);
                    }else{
                        channel = guild.createChannel("kelime-oyunu");
                    }
                    MysqlData.addGuild(guild,channel);
                    MysqlData.initSettings(guild);
                    MysqlData.setChannel(guild, channel);
                    channels.put(guild, channel);
                    BotGUI.addGuild(guild);
                    tryStartGame(guild, channel);
                }catch(MissingPermissionsException e){
                    if(guild!= null && !guild.getStringID().equals("264445053596991498")){
                        guilds.remove(guild);
                    }
                    sendMessage(guild.getDefaultChannel(),false,"Merhaba " +guild.getOwner().mention() + "!\n" + guild.getName() + " sunucuna Kelime Oyunlarını yönetmek için" +
                            " geldim fakat bana gerekli izinleri vermediğin sürece bu işimi yerine getiremem.\n" +
                            "Bana Gerekli olan yetkiler;\n" +
                            "`Kanalları Yönetme (MANAGE_CHANNELS)\n" +
                            "Mesajları Yönetme (MANAGE_MESSAGES)\n" +
                            "Mesaj Gönderme (SEND_MESSAGES)\n" +
                            "Kanalları Görme (VIEW_CHANNEL)\n" +
                            "Sunucuyu Yönetme (MANAGE_CHANNEL) [Sunucu ayarlarını hiçbir şekilde ellemiyorum sadece istatistik toplamak için lazım.]\n" +
                            "Herkesi Etiketleme (MENTION_EVERYONE)`\n" +
                            "Gerekli yetkileri verdiysen " + MysqlData.getPrefix(guild) +"`setup` yaz ki kuruluma başlayabileyim.");

                }
            }
        }

    }

    public static IChannel getGameChannel(IGuild guild){
        return MysqlData.getChannel(guild);
    }

    public static String getTotalUser(){
        totalUser = 0;
        for(IGuild g : guilds){
            totalUser += g.getUsers().size();
        }
        DecimalFormat df = new DecimalFormat("###,###,###");
        String Str5 = df.format(totalUser);
        return Str5;
    }

    private static void tryStartGame(IGuild guild,IChannel channel){
        IChannel rawchannel = channel;
        if(KelimeBot.cl.isReady()){
            totalUser += guild.getUsers().size();
            KelimeBot.startGameForGuild(guild,channel);
            BotMemory.startGame(guild);
            KelimeBot.cl.changePresence(StatusType.ONLINE, ActivityType.PLAYING,getTotalUser() + " Kişi ile Kelime Oyunu ");
        }else{
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            tryStartGame(guild,rawchannel);
                        }
                    },
                    3000
            );
        }
    }

    static <T> List<List<T>> choppedList(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }
}
