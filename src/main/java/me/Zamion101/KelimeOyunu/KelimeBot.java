package me.Zamion101.KelimeOyunu;

import me.Zamion101.KelimeOyunu.experimentals.commands.*;
import me.Zamion101.KelimeOyunu.experimentals.commands.admin.BotStaticsAdminCommand;
import me.Zamion101.KelimeOyunu.experimentals.commands.admin.GuildBlacklistAdminCommand;
import me.Zamion101.KelimeOyunu.experimentals.commands.admin.GuildWhitelistAdminCommand;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.CommandAPI;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import me.Zamion101.KelimeOyunu.experimentals.utils.GuildMemory;
import me.Zamion101.KelimeOyunu.listener.*;
import me.Zamion101.ZamiAPI.main.ZamiAPI;
import me.Zamion101.ZamiAPI.mysql.Mysql;
import me.Zamion101.ZamiAPI.mysql.MysqlAPI;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.handle.impl.events.guild.member.UserLeaveEvent;
import sx.blah.discord.handle.impl.obj.Guild;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

import java.awt.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class KelimeBot {


                                /*[VARIABLES]*/
    static ClientBuilder client;
    public static IDiscordClient cl;
    public static KelimeBot instance;

    public static long startTime = new Date().getTime();

    public static Mysql mysql;
    public static Connection databaseCon;


                                /*[CONSTRUCTOR]*/
    public KelimeBot() {
        instance = this;
        try {
            /*This MySQL system made with ZamiAPI [MySQL API] https://gitlab.com/Zamion101/ZamiAPI */
            /*Vars.java not implemented because of security!*/
            mysql = Vars.MYSQL;
            databaseCon = mysql.openDatabaseConnection("kelime_botu").getConnection("kelime_botu");
        } catch (SQLException e) {
            e.printStackTrace();
        }


        client = new ClientBuilder();
        //client.withToken(Vars.MAIN_TOKEN); //MAIN BOT
        client.withToken(Vars.DEVELOPMENT_TOKEN); //DEVELOPMENT BOT
        client.setPresence(StatusType.ONLINE, ActivityType.PLAYING,"0 Kişi ile Kelime Oyunu ");
        client.build();
        cl = client.login();
        EventDispatcher dispatcher = cl.getDispatcher();


                                /*[NORMAL COMMANDS]*/
        CommandAPI.registerCommand(new UptimeCommand("uptime"));
        CommandAPI.registerCommand(new SetupCommand("kurulum"));
        CommandAPI.registerCommand(new ForceEndGameCommand("bitir"));
        CommandAPI.registerCommand(new StartGameCommand("başlat"));
        CommandAPI.registerCommand(new LeaderboardCommand("sıralama"));
        CommandAPI.registerCommand(new SetPrefixCommand("ayarla-prefix"));
        CommandAPI.registerCommand(new ResetLeaderboardCommand("sıfırla-sıralama"));
        CommandAPI.registerCommand(new SetWordLimitCommand("ayarla-sınır"));
        CommandAPI.registerCommand(new StatisticsCommand("istatistik"));
        CommandAPI.registerCommand(new InfoCommand("yardım"));

                                /*[ADMIN COMMANDS]*/
        CommandAPI.registerCommand(new BotStaticsAdminCommand("admin-sunucu"));
        CommandAPI.registerCommand(new GuildBlacklistAdminCommand("admin-blacklist"));
        CommandAPI.registerCommand(new GuildWhitelistAdminCommand("admin-whitelist"));

        CommandAPI.initCommands();

                                /*[EVENTS]*/
        dispatcher.registerListener(new OnJoinGuild());
        dispatcher.registerListener(new MessageListener());
        dispatcher.registerListener(new OnGuildLeaveEvent());
        dispatcher.registerListener(new DisconnectEvent());
        dispatcher.registerListener(new OnUserLeaveEvent());

        BotMemory.initBitirme();

        setPresence(0);

    }

                                /*[FUNCTIONS]*/
    public static void startGameForGuild(IGuild g,IChannel ch){
        try{
            System.out.println("Starting game for " + g.getName() + " named guild! Guild user size: " + g.getUsers().size() + "\n\tGuild Links: "+ Arrays.toString(g.getExtendedInvites().toArray()));
            System.out.println("Total User Size: " + BotUtils.getTotalUser());
            BotUtils.sendMessage(ch,false,Vars.formatMessage(Vars.BOT_STARTED));
            BotMemory.clearStage(g.getLongID());
            String rand = RandomStringGenerator.generateRandomString(1);
            BotMemory.addWord(g.getLongID(),rand);
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            sendStartEmbed(ch,rand);
                        }
                    },
                    3000
            );
        }catch (MissingPermissionsException e){
            if(BotMemory.isBlacklisted(g)) return;
            String prefix = GuildMemory.getGuild(g).getPrefix();
            g.getOwner().getOrCreatePMChannel().sendMessage("Merhaba " +g.getOwner().mention() + "!\nSahibi olduğun `" + g.getName() + "` adlı sunucuna Kelime Oyunlarını yönetmek için" +
                    " geldim fakat bana gerekli izinleri vermediğin sürece bu işimi yerine getiremem.\n" +
                    "Bana Gerekli olan yetkiler;\n" +
                    "`Kanalları Yönetme (MANAGE_CHANNELS)\n" +
                    "Mesajları Yönetme (MANAGE_MESSAGES)\n" +
                    "Mesaj Gönderme (SEND_MESSAGES)\n" +
                    "Kanalları Görme (VIEW_CHANNEL)\n" +
                    "Sunucuyu Yönetme (MANAGE_CHANNEL) [Sunucu ayarlarını hiçbir şekilde ellemiyorum sadece istatistik toplamak için lazım.]\n" +
                    "Herkesi Etiketleme (MENTION_EVERYONE)`\n" +
                    "Gerekli yetkileri verdiysen `" + prefix + "kurulum` yaz ki kuruluma başlayabileyim.");
        }

    }

    public static void sendStartEmbed(IChannel channel,String rand){
        EmbedBuilder builder = new EmbedBuilder();

        builder.appendField("Rastgele Başlangıç Harfi", rand.toUpperCase(new Locale("tr","TR")), true);
        builder.appendField("Geliştirici", "Zamion101#0349", true);
        builder.appendField("Oyun Dışı Konuşma", "Oyun dışı konuşma için '('mesaj')' kullanmayı unutmayın!", false);
        builder.appendField(":inbox_tray: Sunucuna Davet Et","[Davet Linki](https://www.zamibot.minestand.network)",false);

        builder.withAuthorName("Kelime Oyunu");

        builder.withColor(255, 125, 0);
        builder.withTitle("Rastgele Başlangıç Harfi Belirlendi!");
        builder.withTimestamp(new Date().getTime());

        builder.withFooterText("Kelime Oyunu Botu by Zamion101#0349");
        //builder.withThumbnail("https://i.hizliresim.com/k68aM9.jpg");

        RequestBuffer.request(() -> channel.sendMessage(builder.build()));
    }

    public static void sendEndEmbed(IChannel channel, IUser finished,String word,int type){
        EmbedBuilder builder = new EmbedBuilder();

        builder.appendField("Bitiren", finished.mention(), true);
        builder.appendField("Bitirilen Kelime", word, true);
        builder.appendField("Geliştirici", "Zamion101#0349", false);
        if(type == 0){
            builder.appendField("Yeni Oyun Birazdan Başlayacak!","3 Saniye Bekleyin.",false);
        }else{
            IGuild guild = channel.getGuild();
            String prefix = GuildMemory.getGuild(guild).getPrefix();
            builder.appendField("Yeni Oyun?","Yeni oyunu başlatmak için `" + prefix + "` yazın!",false);
        }
        builder.appendField(":inbox_tray: Sunucuna Davet Et","[Davet Linki](https://www.zamibot.minestand.network)",false);

        builder.withAuthorName("Kelime Oyunu");

        builder.withColor(0, 255, 0);
        builder.withTitle(":tada: Oyun Bitti :tada:");
        builder.withTimestamp(new Date().getTime());

        builder.withFooterText("Kelime Oyunu Botu by Zamion101#0349");
        //builder.withThumbnail("https://i.hizliresim.com/k68aM9.jpg");

        RequestBuffer.request(() -> channel.sendMessage(builder.build()));

    }

    static int t = 0;
    public static void setPresence(int type){
        if(type == 1){
            KelimeBot.cl.changePresence(StatusType.ONLINE, ActivityType.PLAYING,BotUtils.getTotalUser() + " Kişi ile Kelime Oyunu ");
            return;
        }
        new java.util.Timer("Presence").schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        if(t == 0){
                            KelimeBot.cl.changePresence(StatusType.ONLINE, ActivityType.PLAYING,BotUtils.getTotalUser() + " Kişi ile Kelime Oyunu ");
                            t = 1;
                        }else if(t == 1){
                            KelimeBot.cl.changePresence(StatusType.IDLE, ActivityType.PLAYING,BotUtils.getGuildSize() + " Sunucuda Aktifim...");
                            t = 2;
                        }else{
                            KelimeBot.cl.changePresence(StatusType.DND, ActivityType.PLAYING,"Yakında Yeni Özellikler Gelecek.");
                            t = 0;
                        }
                        setPresence(0);
                    }
                },
                30000
        );
    }

    public static Thread getThreadByName(String threadName) {
        for (Thread t : Thread.getAllStackTraces().keySet()) {
            if (t.getName().equals(threadName)) return t;
        }
        return null;
    }

}
