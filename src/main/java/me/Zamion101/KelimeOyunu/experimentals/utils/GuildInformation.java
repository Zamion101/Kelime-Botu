package me.Zamion101.KelimeOyunu.experimentals.utils;

import me.Zamion101.KelimeOyunu.KelimeBot;
import sx.blah.discord.handle.obj.IGuild;

import javax.xml.transform.Result;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GuildInformation {


    String guild_name;
    String guild_id;
    String guild_owner;
    boolean oto_start;
    int guild_size;
    String prefix;

    public GuildInformation(IGuild guild){
        this.guild_name = guild.getName();
        this.guild_id = guild.getStringID();
        this.guild_owner = guild.getOwner().getStringID();
        this.guild_size = guild.getTotalMemberCount();
        getData();
    }

    private void getData(){
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("SELECT * FROM guild_settings WHERE guild_id = " + guild_id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.oto_start = rs.getBoolean("oto_start");
                this.prefix = rs.getString("prefix");
            }else{
                this.oto_start = false;
                System.err.println("Can't get any guild data about '" + this.guild_name + "' from database!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getGuild_name() {
        return guild_name;
    }

    public String getGuild_id() {
        return guild_id;
    }

    public String getGuild_owner() {
        return guild_owner;
    }

    public boolean isOto_start() {
        return oto_start;
    }

    public int getGuild_size() {
        return guild_size;
    }

    public String getPrefix() {
        return prefix;
    }
}
