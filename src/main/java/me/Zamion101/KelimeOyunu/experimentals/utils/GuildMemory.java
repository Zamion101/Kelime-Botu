package me.Zamion101.KelimeOyunu.experimentals.utils;

import sx.blah.discord.handle.obj.IGuild;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class GuildMemory {


    static Map<String,GuildInformation> guilds = new HashMap<>();

    public static void addGuild(IGuild guild){
        if(guilds.containsKey(new String(guild.getName().getBytes(), Charset.forName("UTF-8")))) return;
        guilds.put(new String(guild.getName().getBytes(),Charset.forName("UTF-8")),new GuildInformation(guild));
    }

    public static GuildInformation getGuild(IGuild guild){
        if(!guilds.containsKey(new String(guild.getName().getBytes(),Charset.forName("UTF-8")))){
            addGuild(guild);
        }
        return guilds.get(new String(guild.getName().getBytes(),Charset.forName("UTF-8")));
    }

    public static GuildInformation getGuild(String guildName){
        return guilds.get(guildName);
    }
}
