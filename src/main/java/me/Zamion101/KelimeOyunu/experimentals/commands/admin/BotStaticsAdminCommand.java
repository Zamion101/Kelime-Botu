package me.Zamion101.KelimeOyunu.experimentals.commands.admin;

import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.Vars;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;

public class BotStaticsAdminCommand extends KOCommand<MessageReceivedEvent> {

    public BotStaticsAdminCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event, String[] args) {
        IGuild guild = event.getGuild();
        IUser user = event.getAuthor();
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();

        if(user.getStringID().equals("117331106868625411")){
            message.delete();
            EmbedBuilder builder = new EmbedBuilder();
            builder.withTimestamp(new Date().getTime());
            builder.withTitle("[Bot Sahibi] " + new String(guild.getName().getBytes(), Charset.forName("UTF-8")) + " istatistikler.");

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("-----------------------------\n");
            stringBuilder.append("Sunucu Adı => " + guild.getName());
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu ID => " + guild.getStringID());
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Sahibi => " + guild.getOwner().mention());
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Sahibi ID => " + guild.getOwner().getStringID());
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Üye Sayısı => " + guild.getUsers().size());
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Icon URL => " + guild.getIconURL());
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Emojileri => " + Arrays.toString(guild.getEmojis().toArray()));
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Bölgesi => " + guild.getRegion().getName());
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Bölgesi VIP Durumu => " + guild.getRegion().isVIPOnly());
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Konuşma Kanalları => " + Arrays.toString(guild.getVoiceChannels().toArray()));
            stringBuilder.append("\n");
            stringBuilder.append("Sunucu Doğrulama Durumu => " + guild.getVerificationLevel().name());
            stringBuilder.append("\n-----------------------------");

            builder.appendField("Durum",stringBuilder.toString(),false);

            builder.withAuthorName("Kelime Botu");
            builder.withColor(50,100,150);
            RequestBuffer.request(() -> user.getOrCreatePMChannel().sendMessage(builder.build()));
            return true;
        }else{
            message.delete();
            BotUtils.sendMessage(channel,true,10, Vars.formatMessage(Vars.NO_ADMIN_PERMISSION,user.mention()));
            return true;
        }
    }
}
