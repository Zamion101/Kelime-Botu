package me.Zamion101.KelimeOyunu.experimentals.commands;

import com.sun.org.apache.xml.internal.utils.LocaleUtility;
import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import me.Zamion101.KelimeOyunu.experimentals.utils.GuildMemory;
import sun.util.resources.tr.LocaleNames_tr;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class LeaderboardCommand extends KOCommand<MessageReceivedEvent> {

    public LeaderboardCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event, String[] args) {
        IGuild guild = event.getGuild();
        IChannel channel = event.getChannel();
        EmbedBuilder builder = new EmbedBuilder();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sıra | Kullanıcı Adı\n");
        stringBuilder.append("\n------------------------------------------\n");
        Map<IUser,Integer> data = MysqlData.getLeaderboard(guild);
        int rank = 1;
        int $rank = 0;
        int $score = 0;
        for(IUser user : data.keySet()){
            if(rank > 9){
                break;
            }
            int score = data.get(user);
            if(user.getStringID().equals(event.getAuthor().getStringID())){
                $rank = rank;
                $score = score;
            }
            stringBuilder.append("[" + rank + "]\t> #" + user.getName()+ "\n");
            stringBuilder.append("\tToplam Kazanma: " + score + "\n");
            rank++;
        }
        stringBuilder.append("\n------------------------------------------");
        stringBuilder.append("\n# Senin Sıralaman");
        if(MysqlData.isUserExistInLeaderboard(guild,event.getAuthor())){
            if($rank == 0){
                for(IUser user : data.keySet()){
                    if(user.getStringID().equals(event.getAuthor().getStringID())){
                        $score = data.get(user);
                    }
                }
            }
            stringBuilder.append("\nSıra: " + $rank + " / Toplam Kazanma: " + $score);
        }else{
            stringBuilder.append("\nSıra: Bilinmiyor! / Toplam Kazanma: 0");
        }

        builder.appendField("Skor Tablosu","```" + stringBuilder.toString() + "```",false);
        builder.appendField("Geliştirici", "Zamion101#0349", false);

        builder.appendField(":inbox_tray: Sunucuna Davet Et","[Davet Linki](https://www.zamibot.minestand.network)",false);

        builder.withAuthorName("Kelime Oyunu");

        builder.withColor(125, 255, 125);
        builder.withTitle(new String(guild.getName().getBytes(), Charset.forName("UTF-8")) + " adlı Sunucunun Skor Tablosu");
        builder.withTimestamp(new Date().getTime());

        builder.withFooterText("Kelime Oyunu Botu by Zamion101#0349");

        RequestBuffer.request(() -> channel.sendMessage(builder.build()));

        return true;
    }
}
