package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.Vars;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.PermissionUtils;

public class SetPrefixCommand extends KOCommand<MessageReceivedEvent> {

    public SetPrefixCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event,String[] args) {
        IGuild guild = event.getGuild();
        IUser author = event.getAuthor();
        IMessage msg = event.getMessage();
        IChannel channel = event.getChannel();
        if(PermissionUtils.hasPermissions(guild,author, Permissions.ADMINISTRATOR)){
            try{
                MysqlData.setPrefix(guild,args[0]);
                msg.delete();
                BotUtils.sendMessage(channel,true,Vars.formatMessage(Vars.OK_CHANGE_PREFIX,author.mention(),args[0]));
                return true;
            }catch (Exception e){
                return true;
            }
        }else{
            msg.delete();
            BotUtils.sendMessage(channel,true, Vars.formatMessage(Vars.NO_PERMISSION,author.mention()));
            return true;
        }
    }
}
