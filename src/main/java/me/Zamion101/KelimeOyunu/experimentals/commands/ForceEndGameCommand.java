package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.BotMemory;
import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.Vars;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.PermissionUtils;

public class ForceEndGameCommand extends KOCommand<MessageReceivedEvent> {

    public ForceEndGameCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event,String[] args) {
        IMessage msg = event.getMessage();
        IUser author = event.getAuthor();
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();


        if(PermissionUtils.hasPermissions(guild,author, Permissions.ADMINISTRATOR)){
            if (BotMemory.isGameStarted(guild)){
                IChannel gameChannel = BotUtils.getGameChannel(guild);
                BotUtils.sendMessage(gameChannel,false, Vars.formatMessage(Vars.GAME_STOPPED,author.mention()));
                BotMemory.stopGame(guild);
                msg.delete();
                System.out.printf("Game ended for %s named Guild! Game ended by %s",guild.getName(),author.getName());
                return true;
            }else{
                msg.delete();
                BotUtils.sendMessage(channel,true,Vars.formatMessage(Vars.GAME_ALREADY_STOPPED,author.mention(), MysqlData.getPrefix(guild)));
                return true;
            }

        }else{
            msg.delete();
            BotUtils.sendMessage(channel,true,Vars.formatMessage(Vars.NO_PERMISSION,author.mention()));
            return true;
        }
    }
}
