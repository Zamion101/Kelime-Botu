package me.Zamion101.KelimeOyunu.experimentals.commands.framework;

import sx.blah.discord.api.events.Event;

public abstract class KOCommand<T extends Event> implements IKOCommands<T>{

    String command;

    public KOCommand(String command){
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

}
