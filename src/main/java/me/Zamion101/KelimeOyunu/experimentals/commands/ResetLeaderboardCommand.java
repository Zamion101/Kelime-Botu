package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.Vars;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.PermissionUtils;

import java.sql.SQLException;

public class ResetLeaderboardCommand extends KOCommand<MessageReceivedEvent> {

    public ResetLeaderboardCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event, String[] args) {
        IGuild guild = event.getGuild();
        IUser user = event.getAuthor();
        IChannel channel = event.getChannel();
        IMessage message = event.getMessage();
            if(PermissionUtils.hasPermissions(guild,user, Permissions.ADMINISTRATOR)){
                MysqlData.resetLeaderboard(guild);
                message.delete();
                BotUtils.sendMessage(channel,true, Vars.formatMessage(Vars.RESET_LEADERBOARD,user.mention()));
            }else{
                message.delete();
                BotUtils.sendMessage(channel,true,Vars.formatMessage(Vars.NO_PERMISSION,user.mention()));
                return true;
            }
        return true;
    }
}
