package me.Zamion101.KelimeOyunu.experimentals.commands.framework;

import sx.blah.discord.api.events.Event;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandAPI {

    private static Map<String, KOCommand<Event>> registered = new HashMap<>();
    private static List<KOCommand<Event>> queue = new ArrayList<>();
    private static Map<KOCommand<Event>,String> failed = new HashMap<>();

    public static void registerCommand(Object command){
        queue.add((KOCommand<Event>) command);
    }


    public static void unregisterCommand(String command){
            registered.remove(command);
    }

    public static boolean isCommandExist(String command){
        return registered.containsKey(command);
    }

    public static void resetCommandList(){
        List<KOCommand<Event>> temp_queue = queue;
        System.out.println("\n-+-{Started to Resetting Command List}-+-\n");
        queue.clear();
        for(String reg : registered.keySet()){
            KOCommand<Event> cmd = registered.get(reg);
            queue.add(cmd);
        }
        registered.clear();
        for(KOCommand<Event> cmd : failed.keySet()){
            queue.add(cmd);
        }
        failed.clear();
        for(KOCommand<Event> cmd : temp_queue){
            queue.add(cmd);
        }
        temp_queue.clear();
        System.out.println("\n-+-{Finished the Resetting Command List}-+-\n");
    }

    public static void initCommands(){
        System.out.println("\n-+-{Started to initializing the Commands}-+-\n");
        for(KOCommand<Event> cmd : queue){
            if(registered.containsKey(cmd.getCommand())){
                failed.put(cmd,"Already Registered!" +
                        "\n\tTrying to register Command Class: " + cmd.getCommand().getClass().getSimpleName() +
                        "\n\tAlready registered Command Class: " + new String(registered.get(cmd.getCommand()).getClass().getSimpleName().getBytes(), Charset.forName("UTF-8")));
                continue;
            }
            System.out.println("\t-Command Registered!\n\t\t-Command: " + new String(cmd.getCommand().getBytes(),Charset.forName("UTF-8")) + "\n\t\t\t-Command Class: " + cmd.getClass().getSimpleName() + "\n");
            registered.put(cmd.getCommand(),cmd);
        }
        queue.clear();
        System.out.println("\n-+-{Finished to initializing the Commands}-+-\n");

        System.out.println("\n-+-{Begin of Failed Commands while trying to initialize}-+-\n");
            for(KOCommand<?> cmd : failed.keySet()){
                String error = failed.get(cmd);
                System.out.println("\tCommand: " + cmd.getCommand() + "\n\t" + error);
            }
        System.out.println("\n-+-{End of Failed Commands while trying to initialize}-+-\n");
    }


    public static KOCommand<Event> getCommand(String command){
        if(!registered.containsKey(command)) return null;
        return registered.get(command);
    }


    public static Map<String, KOCommand<Event>> getRegistered() {
        return registered;
    }

    public static Map<KOCommand<Event>, String> getFailedCommands() {
        return failed;
    }

    public static boolean runCommand(KOCommand<Event> command,String[] args, Event event){
        if(failed.containsKey(command))return false;
        if(queue.contains(command))return false;
        return command.handle(event,args);
    }

    public static boolean runCommand(String command,String[] args, Event event){
        if(!registered.containsKey(command)) return false;
        return runCommand(getCommand(command),args,event);
    }
}
