package me.Zamion101.KelimeOyunu.experimentals.commands.admin;

import me.Zamion101.KelimeOyunu.BotMemory;
import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.Vars;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

public class GuildWhitelistAdminCommand extends KOCommand<MessageReceivedEvent> {

    public GuildWhitelistAdminCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event, String[] args) {
        IGuild guild = event.getGuild();
        IUser user = event.getAuthor();
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();

        if(user.getStringID().equals("117331106868625411")){
            if(BotMemory.isBlacklisted(args[0])){
                MysqlData.removeBlacklist(args[0]);
            }
            BotUtils.sendMessage(channel,true,10,Vars.OK_WHITELIST);
        }else{
            message.delete();
            BotUtils.sendMessage(channel,true,10, Vars.formatMessage(Vars.NO_ADMIN_PERMISSION,user.mention()));
            return true;
        }
        return true;
    }
}
