package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.BotMemory;
import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.KelimeBot;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.Date;

public class StatisticsCommand extends KOCommand<MessageReceivedEvent> {

    public StatisticsCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event, String[] args) {
        long pingstart = System.currentTimeMillis();
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();

        message.delete();

        EmbedBuilder builder = new EmbedBuilder();
        builder.withColor(100,50,150);
        builder.withTitle("Kelime Botu İstatistikler");
        StringBuilder sBuilder = new StringBuilder();
        StringBuilder strBuilder = new StringBuilder();

        sBuilder.append("```");
        sBuilder.append("Bot Versiyonu => v1.1.3-BETA");
        sBuilder.append("\n");
        sBuilder.append("Shard Sayisi => " + KelimeBot.cl.getShardCount());
        sBuilder.append("\n");
        sBuilder.append("Sunucu Sayisi => " + KelimeBot.cl.getGuilds().size());
        sBuilder.append("\n");
        sBuilder.append("Kullanıcı Sayisi => " + BotUtils.getTotalUser());
        sBuilder.append("\n");
        sBuilder.append("Bot Sahibi => " + KelimeBot.cl.getApplicationOwner().getName() + "#" + KelimeBot.cl.getApplicationOwner().getDiscriminator());
        sBuilder.append("```");


        Runtime rn =  Runtime.getRuntime();
        strBuilder.append("```");
        strBuilder.append("Sistem Islemcileri => " + rn.availableProcessors());
        strBuilder.append("\n");
        strBuilder.append("Sistem Bos Ram => " + rn.freeMemory()/1000/1000 + " MB");
        strBuilder.append("\n");
        strBuilder.append("Sistem Toplam Ram => " + rn.totalMemory()/1000/1000 + " MB");
        strBuilder.append("\n");
        strBuilder.append("Sistem Maksimum Ram => " + rn.maxMemory()/1000/1000 + " MB");
        strBuilder.append("\n");
        strBuilder.append("Gecikme Suresi => "+ (System.currentTimeMillis() - pingstart) + "ms");
        strBuilder.append("```");
        builder.appendField("Bot Durumu",sBuilder.toString(),false);
        builder.appendField("Sistem Durumu",strBuilder.toString(),false);

        RequestBuffer.request(() -> channel.sendMessage(builder.build()));
        return true;
    }
}
