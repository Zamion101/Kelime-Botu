package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.KelimeBot;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UptimeCommand extends KOCommand<MessageReceivedEvent>{

    public UptimeCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event,String[] args) {
        IChannel channel = event.getChannel();
        sendTimeMessage(channel);
        return true;
    }


    private void sendTimeMessage(IChannel channel){
        EmbedBuilder builder = new EmbedBuilder();
        long[] time = getTimeDiff(new Date(),new Date(KelimeBot.startTime));
        builder.appendField("Gün", String.valueOf(time[0]), true);
        builder.appendField("Saat", String.valueOf(time[1]), true);
        builder.appendField("Dakika", String.valueOf(time[2]), true);
        builder.appendField("Saniye",String.valueOf(time[3]),true);
        builder.appendField("Son Yenileme",getTime(new Date(KelimeBot.startTime),"dd.MM.yyyy - HH:mm:ss (z)"),false);
        builder.appendField("Geliştirici", "Zamion101#0349", false);
        builder.appendField(":inbox_tray: Sunucuna Davet Et","[Davet Linki](https://www.zamibot.minestand.network)",false);


        builder.withAuthorName("Kelime Oyunu");

        builder.withColor(255, 71, 0);
        builder.withTitle(":alarm_clock: Çalışma Süresi Bilgisi :alarm_clock:");
        builder.withTimestamp(new Date().getTime());

        builder.withFooterText("Kelime Oyunu Botu by Zamion101#0349");
        //builder.withThumbnail("https://i.hizliresim.com/k68aM9.jpg");

        RequestBuffer.request(() -> channel.sendMessage(builder.build()));
    }


    private String getTime(Date date, String pattern) {
        return new SimpleDateFormat(pattern).format(date);
    }

    public static long[] getTimeDiff(Date date1, Date date2) {
        long diff = date1.getTime() - date2.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        long[] longs = new long[5];
        longs[0] = Long.parseLong(parseTimeNumbs(diffDays));
        longs[1] = Long.parseLong(parseTimeNumbs(diffHours));
        longs[2] = Long.parseLong(parseTimeNumbs(diffMinutes));
        longs[3] = Long.parseLong(parseTimeNumbs(diffSeconds));
        longs[4] = Long.parseLong(parseTimeNumbs(diff));
        return longs;
    }

    private static String parseTimeNumbs(long time) {
        String timeString = time + "";
        if (timeString.length() < 2)
            timeString = "0" + time;
        return timeString;
    }



    public static void sendTime(){
        long startTime = KelimeBot.startTime;
        long commandTime = new Date().getTime();

        long uptimeTime = commandTime - startTime;

        Date date = new Date(uptimeTime);
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd--kk--mm--ss");
        String formatted = DATE_FORMAT.format(date);
        String[] reFormat = formatted.split("--");
        int d = Integer.parseInt(reFormat[0]);
        int h = Integer.parseInt(reFormat[1]);
        int m = Integer.parseInt(reFormat[2]);
        int s = Integer.parseInt(reFormat[3]);
        System.out.println(d + " Gün " + h + " Saat " + m + " Dakika " + s + " Saniye ");
    }
}
