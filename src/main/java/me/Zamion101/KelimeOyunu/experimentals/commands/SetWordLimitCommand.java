package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.Vars;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.PermissionUtils;

public class SetWordLimitCommand extends KOCommand<MessageReceivedEvent> {

    public SetWordLimitCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event, String[] args) {
        IGuild guild = event.getGuild();
        IUser author = event.getAuthor();
        IMessage msg = event.getMessage();
        IChannel channel = event.getChannel();
        if(PermissionUtils.hasPermissions(guild,author, Permissions.ADMINISTRATOR)){
            if(isInteger(args[0])){
                msg.delete();
                int lim = Integer.parseInt(args[0]);
                MysqlData.setWordLimit(guild,lim);
                BotUtils.sendMessage(channel,true, Vars.formatMessage(Vars.OK_NEW_LIMIT,author.mention(),lim));
            }else{
                BotUtils.sendMessage(channel,true,10,Vars.formatMessage(Vars.MUST_BE_INTEGER,author.mention(),args[0],MysqlData.getPrefix(guild)));
            }
        }else{
        msg.delete();
        BotUtils.sendMessage(channel,true,Vars.formatMessage(Vars.NO_PERMISSION,author.mention()));
        return true;
    }

        return false;
    }

    private static boolean isInteger(String str){
        try{
            Integer.parseInt(str);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
