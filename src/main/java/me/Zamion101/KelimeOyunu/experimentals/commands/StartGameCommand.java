package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.BotMemory;
import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.Vars;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.PermissionUtils;

import java.nio.charset.Charset;

public class StartGameCommand extends KOCommand<MessageReceivedEvent> {

    public StartGameCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event,String[] args) {
        IMessage msg = event.getMessage();
        IUser author = event.getAuthor();
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();
        IChannel gameChannel = BotUtils.getGameChannel(guild);

        if(PermissionUtils.hasPermissions(guild,author, Permissions.ADMINISTRATOR)){
            if(!BotMemory.isGameStarted(guild)){
                BotUtils.sendMessage(gameChannel,false,Vars.formatMessage(Vars.GAME_RESTARTED,author.mention()));
                new java.util.Timer().schedule(
                        new java.util.TimerTask() {
                            @Override
                            public void run() {
                                msg.delete();
                                BotUtils.addGuild(guild, new String(MysqlData.getChannel(guild).getName().getBytes(), Charset.forName("UTF-8")));
                            }
                        },
                        3000
                );
                return true;
            }else{
                msg.delete();
                BotUtils.sendMessage(channel,true,Vars.formatMessage(Vars.GAME_ALREADY_STARTED,author.mention(),MysqlData.getPrefix(guild)));
                return true;
            }
        }else{
            msg.delete();
            BotUtils.sendMessage(channel,true, Vars.formatMessage(Vars.NO_PERMISSION,author.mention()));
            return true;
        }
    }
}
