package me.Zamion101.KelimeOyunu.experimentals.commands.framework;

import sx.blah.discord.api.events.Event;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

public interface IKOCommands<T extends Event> {

    boolean handle(T event, String[] args);
}
