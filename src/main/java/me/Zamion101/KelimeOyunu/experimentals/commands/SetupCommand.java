package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.BotMemory;
import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.Vars;
import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import me.Zamion101.KelimeOyunu.experimentals.mysql.MysqlData;
import me.Zamion101.KelimeOyunu.experimentals.utils.GuildMemory;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.PermissionUtils;

public class SetupCommand extends KOCommand<MessageReceivedEvent>{

    public SetupCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event,String[] args) {
        IMessage msg = event.getMessage();
        IUser author = event.getAuthor();
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();
    try {
        if (PermissionUtils.hasPermissions(guild,author, Permissions.ADMINISTRATOR)) {
            if(args.length > 0){
                BotUtils.addGuild(guild,args[0]);
                msg.delete();
                return true;
            }
            if(guild.getChannelsByName("kelime-oyunu") == null  || MysqlData.getChannel(guild) == null){
                BotUtils.addGuild(guild,null);
                msg.delete();
                return true;
            }else{
                msg.delete();
                BotUtils.sendMessage(channel,true,Vars.formatMessage(Vars.EVERYTHING_OK,author.mention()));
                return true;
            }
        }else{
            msg.delete();
            BotUtils.sendMessage(channel,true, Vars.formatMessage(Vars.NO_PERMISSION,author.mention()));
            return true;
        }
    }catch (MissingPermissionsException e){
        String prefix = GuildMemory.getGuild(guild).getPrefix();
        BotUtils.sendMessage(channel,false,"Merhaba " +channel.getGuild().getOwner().mention() +"!\nSahibi olduğun `" + channel.getGuild().getName() + "` adlı sunucuna Kelime Oyunlarını yönetmek için" +
                " geldim fakat bana gerekli izinleri vermediğin sürece bu işimi yerine getiremem.\n" +
                "Bana Gerekli olan yetkiler;\n" +
                "`Kanalları Yönetme (MANAGE_CHANNELS)\n" +
                "Mesajları Yönetme (MANAGE_MESSAGES)\n" +
                "Mesaj Gönderme (SEND_MESSAGES)\n" +
                "Kanalları Görme (VIEW_CHANNEL)\n" +
                "Sunucuyu Yönetme (MANAGE_CHANNEL) [Sunucu ayarlarını hiçbir şekilde ellemiyorum sadece istatistik toplamak için lazım.]\n" +
                "Herkesi Etiketleme (MENTION_EVERYONE)`\n" +
                "Gerekli yetkileri verdiysen `" + prefix + "kurulum` yaz ki kuruluma başlayabileyim.");
        /**BotUtils.sendMessage(channel,true,5,"I don't have enough permission to run perfectly. This perrmissions what i need to work perfectly;\n" +
         "`Manage Channels (MANAGE_CHANNELS)\n" +
         "Manage Messages (MANAGE_MESSAGES)\n" +
         "Sending Messages (SEND_MESSAGES)\n" +
         "View Messages (VIEW_CHANNEL)\n" +
         "Mention About User (MENTION_EVERYONE)`\n" +
         "If already set the permissions please use `ko!setup` command to setup game.");*/
        return true;
    }
    }
}
