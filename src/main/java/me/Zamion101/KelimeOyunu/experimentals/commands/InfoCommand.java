package me.Zamion101.KelimeOyunu.experimentals.commands;

import me.Zamion101.KelimeOyunu.experimentals.commands.framework.KOCommand;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.awt.*;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.Date;

public class InfoCommand extends KOCommand<MessageReceivedEvent>{

    public InfoCommand(String command) {
        super(command);
    }

    @Override
    public boolean handle(MessageReceivedEvent event, String[] args) {
        event.getMessage().delete();
        EmbedBuilder builder = new EmbedBuilder();

        builder.withTitle("Kelime Botu");
        builder.withColor(25,75,225);
        builder.withTimestamp(new Date().getTime());
        builder.withFooterText("Kelime Botu by Zamion101#0349");

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder strBuilder = new StringBuilder();

        stringBuilder.append("**ayarla-sınır**: ayarla-sınır <Sayı> #Bu komut ile oyun bitmeden önce kaç tane kelimenin yazılması zorunda olduğunu ayarlayabilirsiniz.");
        stringBuilder.append("\n");
        stringBuilder.append("**ayarla-prefix**: ayarla-prefix <Prefix> #Bu komut ile komutların önüne gelen belirteci ayarlayabilirsiniz. (Varsayılan prefix 'ko!')");
        stringBuilder.append("\n");
        stringBuilder.append("**kurulum**: kurulum [Kanal Adı] #Bu komut ile Kelime Oyunu'nun hangi kanalda oynanacağını seçebilirsiniz.");
        stringBuilder.append("\n");
        strBuilder.append("**sıfırla-sıralama**: #Bu komut ile skor tablosunu sıfırlayabilirsiniz.");

        stringBuilder.append("**bitir**: #Bu komut ile oyunu zorla durdurabilirsiniz.");
        stringBuilder.append("\n");
        stringBuilder.append("**başlat**: #Bu komut ile zorla durdurulmuş oyunu başlatabilirsniz.");
        stringBuilder.append("\n");
        stringBuilder.append("**sıralama**: #Bu komut ile sunucu çaplı skor tablosuna bakabilirsiniz.");
        stringBuilder.append("\n");
        stringBuilder.append("**istatistik**: #Bu komut ile botun istatistiklerine bakabilirsiniz.");
        stringBuilder.append("\n");
        stringBuilder.append("**uptime**: #Bu komut ile botun çalışma süresine ve son yenileme zamanına bakabilirsiniz.");

        builder.appendField(":tools: Ayar Komutları",stringBuilder.toString(),false);
        builder.appendField(":black_joker: Oyun Komutları",stringBuilder.toString(),false);

        RequestBuffer.request(() -> event.getAuthor().getOrCreatePMChannel().sendMessage(builder.build()));
        return true;
    }
}
