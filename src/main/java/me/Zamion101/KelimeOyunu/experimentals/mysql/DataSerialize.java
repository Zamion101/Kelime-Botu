package me.Zamion101.KelimeOyunu.experimentals.mysql;

import com.sun.org.apache.bcel.internal.generic.IUSHR;
import me.Zamion101.KelimeOyunu.KelimeBot;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DataSerialize implements Serializable{


    public static String serialize(Map<IUser,Integer> data){
        StringBuilder stringBuilder = new StringBuilder();
        for(IUser sub : data.keySet()){
            int score = data.get(sub);
            stringBuilder.append(sub.getStringID()+";"+ score +"::");
        }
        return stringBuilder.toString();
    }


    public static Map<IUser,Integer> deserialize(String guildID,String serializedString){
        IGuild guild = KelimeBot.cl.getGuildByID(Long.valueOf(guildID));
        Map<IUser,Integer> data = new HashMap<>();
        for(String ser : serializedString.split("::")){
            String[] sub = ser.split(";");
            if(sub[0].equals("")){
                break;
            }
            if(guild.getUserByID(Long.valueOf(sub[0])) == null){
                continue;
            }
            IUser user = guild.getUserByID(Long.valueOf(sub[0]));
            int score = Integer.valueOf(sub[1] != "" ? sub[1] : "0");
            data.put(user,score);
        }
        return data;
    }
}
