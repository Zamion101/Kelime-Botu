package me.Zamion101.KelimeOyunu.experimentals.mysql;

import com.sun.org.apache.bcel.internal.generic.IUSHR;
import me.Zamion101.KelimeOyunu.BotMemory;
import me.Zamion101.KelimeOyunu.KelimeBot;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static java.util.stream.Collectors.toMap;

public class MysqlData {

    public static IChannel getChannel(String guild){
        PreparedStatement ps;
        IGuild $guild = KelimeBot.cl.getGuildByID(Long.valueOf(guild));
        try {
            ps = KelimeBot.databaseCon.prepareStatement("SELECT * FROM guild_channels WHERE guild = " + guild);
            ResultSet rs = ps.executeQuery();
            if(!rs.next()){
                return null;
            }
            String channelID = rs.getString("guild_channel");
            return $guild.getChannelByID(Long.valueOf(channelID));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static IChannel getChannel(IGuild guild){
        return getChannel(guild.getStringID());
    }


    public static void setChannel(IGuild guild,IChannel channel){
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("UPDATE guild_channels  SET guild_channel = " + channel.getStringID() +" WHERE guild = " + guild.getStringID());
            ps.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void addGuild(IGuild guild,IChannel channel){
        if(isGuildExist(guild));
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("INSERT INTO guild_channels (guild,guild_channel) VALUES (?,?)");
            ps.setString(1,guild.getStringID());
            ps.setString(2,channel.getStringID());
            ps.executeLargeUpdate();
            ps = KelimeBot.databaseCon.prepareStatement("INSERT INTO guild_leaderboard (guild,guild_serialized) VALUES (?,?)");
            ps.setString(1,guild.getStringID());
            ps.setString(2,"");
            ps.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void initSettings(IGuild guild){
        if(isGuildExist(guild)){ System.out.println("RETURN"); return;}
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("INSERT INTO guild_settings (guild_id,guild_owner,oto_start,prefix) VALUES (?,?,?,?)");
            ps.setString(1,guild.getStringID());
            ps.setString(2,guild.getOwner().getStringID());
            ps.setBoolean(3,true);
            ps.setString(4,"ko!");
            ps.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void addBlacklist(String guildID){
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("INSERT INTO guild_blacklist (guild) VALUES (?)");
            ps.setString(1,guildID);
            ps.executeLargeUpdate();
            BotMemory.resetBlacklisted();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getBlacklist(){
        PreparedStatement ps;
        List<String> bl = new ArrayList<>();
        try {
            ps = KelimeBot.databaseCon.prepareStatement("SELECT * FROM guild_blacklist");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                bl.add(rs.getString("guild"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bl;
    }

    public static void removeBlacklist(String guildID){
        PreparedStatement ps;

        try {
            ps = KelimeBot.databaseCon.prepareStatement("DELETE  FROM guild_blacklist WHERE guild = " + guildID);
            ps.executeLargeUpdate();
            BotMemory.resetBlacklisted();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void removeGuild(IGuild guild){
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("DELETE FROM guild_settings WHERE guild_id = " + guild.getStringID());
            ps.executeLargeUpdate();
            ps = KelimeBot.databaseCon.prepareStatement("DELETE FROM guild_leaderboard WHERE guild = " + guild.getStringID());
            ps.executeLargeUpdate();
            ps = KelimeBot.databaseCon.prepareStatement("DELETE FROM guild_channels WHERE guild = " + guild.getStringID());
            ps.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isGuildExist(IGuild guild){
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("SELECT * FROM guild_settings WHERE guild_id = " + guild.getStringID());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getPrefix(IGuild guild){
        PreparedStatement ps;
        try{
            ps = KelimeBot.databaseCon.prepareStatement("SELECT * FROM guild_settings WHERE guild_id = " + guild.getStringID());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                return rs.getString("prefix");
            }
            return "ko!";
        }catch (Exception e) {
            e.printStackTrace();
            return "ko!";
        }
    }

    public static void setPrefix(IGuild guild,String prefix){
        PreparedStatement ps;
        try{
            ps = KelimeBot.databaseCon.prepareStatement("UPDATE guild_settings SET prefix = '" + prefix +"' WHERE guild_id = " + guild.getStringID());
            ps.executeLargeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getSerializedLeaderboard(String guild){
        return getSerializedLeaderboard(KelimeBot.cl.getGuildByID(Long.valueOf(guild)));
    }

    public static String getSerializedLeaderboard(IGuild guild){
        PreparedStatement ps;

        try {
            ps = KelimeBot.databaseCon.prepareStatement("SELECT * FROM guild_leaderboard WHERE guild = " + guild.getStringID());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                String serialized = rs.getString("guild_serialized");
                return serialized;
            }
            return "";
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public static void setSerializedLeaderboard(IGuild guild,String serialized){
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("UPDATE guild_leaderboard SET guild_serialized ='" + serialized + "' WHERE guild = " + guild.getStringID());
            ps.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addUserToBoard(IGuild guild, IUser user){
        Map<IUser,Integer> data = DataSerialize.deserialize(guild.getStringID(),getSerializedLeaderboard(guild));
        if(isUserExistInLeaderboard(data,user)){
            addScore(guild,data,user);
            return;
        }
        data.put(user,1);
        String serialized = DataSerialize.serialize(data);
        setSerializedLeaderboard(guild,serialized);
    }

    public static int getWordLimit(IGuild guild){
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("SELECT * FROM guild_settings WHERE guild_id = " + guild.getStringID());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                int lim = rs.getInt("kelime_sinir");
                return lim;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 20;
    }

    public static void setWordLimit(IGuild guild,int limit){
        PreparedStatement ps;
        try {
            ps = KelimeBot.databaseCon.prepareStatement("UPDATE guild_settings SET kelime_sinir = " + limit + " WHERE guild_id = " + guild.getStringID());
            ps.executeLargeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void addScore(IGuild guild,Map<IUser,Integer> data,IUser user){
        int score = data.get(user) + 1;
        data.remove(user);
        data.put(user,score);
        String serialized = DataSerialize.serialize(data);
        setSerializedLeaderboard(guild,serialized);
    }

    public static void resetLeaderboard(IGuild guild){
        setSerializedLeaderboard(guild,"");
    }

    public static void removeUser(IGuild guild,IUser user){
        Map<IUser,Integer> data = DataSerialize.deserialize(guild.getStringID(),getSerializedLeaderboard(guild));
        List<IUser> users = new ArrayList<>();
        Map<IUser,Integer> newData = new HashMap<>();
        for(IUser u : data.keySet()){
            if(u.getStringID().equals(user.getStringID())){
            continue;
            }
            users.add(user);
        }
        for(IUser u : users){
            int score = data.get(u);
            newData.put(u,score);
        }
        if(newData.size() == 0 || newData.isEmpty()){
            setSerializedLeaderboard(guild,"");
            return;
        }
        setSerializedLeaderboard(guild,DataSerialize.serialize(newData));
    }

    public static boolean isUserExistInLeaderboard(Map<IUser,Integer> data,IUser user){
        return data.containsKey(user);
    }

    public static boolean isUserExistInLeaderboard(IGuild guild,IUser user){
        Map<IUser,Integer> data = DataSerialize.deserialize(guild.getStringID(),getSerializedLeaderboard(guild));
        return data.containsKey(user);
    }


    public static Map<IUser,Integer> getLeaderboard(IGuild guild){
        Map<IUser,Integer> data = DataSerialize.deserialize(guild.getStringID(),getSerializedLeaderboard(guild));
        Map<IUser,Integer> sorted =  data
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));
        return sorted;
    }
}



class MyComparator implements Comparator {
    Map map;

    public MyComparator(Map map) {
        this.map = map;
    }

    @Override
    public int compare(Object o1, Object o2) {
        return (o2.toString()).compareTo(o1.toString());
    }
}
