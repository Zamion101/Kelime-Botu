package me.Zamion101.KelimeOyunu.GUI.utils;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import me.Zamion101.KelimeOyunu.BotUtils;

public class GuildData {

    private final StringProperty guildName;
    private StringProperty guildSize;
    private StringProperty totalUserSize;
    private StringProperty totalGuildSize;


    public GuildData() {
        this(null,null);
    }

    public GuildData(String guildName, String guildSize) {
        this.guildName = new SimpleStringProperty(guildName);
        this.guildSize = new SimpleStringProperty(guildSize);
        this.totalGuildSize = new SimpleStringProperty(String.valueOf(BotUtils.getGuildSize()));
        this.totalUserSize = new SimpleStringProperty(String.valueOf(BotUtils.getTotalUser()));
    }


    public String getGuildName() {
        return guildName.get();
    }

    public String getGuildSize() {
        return guildSize.get();
    }


    public StringProperty guildNameProperty() {
        return guildName;
    }

    public StringProperty guildSizeProperty() {
        return guildSize;
    }

    public void setTotalGuildSize(String totalGuildSize) {
        this.totalGuildSize.set(totalGuildSize);
    }

    public void setTotalUserSize(String totalUserSize) {
        this.totalUserSize.set(totalUserSize);
    }

    public void setGuildSize(String guildSize) {
        this.guildSize.set(guildSize);
    }

    public String getTotalGuildSize() {
        return totalGuildSize.get();
    }

    public String getTotalUserSize() {
        return totalUserSize.get();
    }

    public StringProperty totalGuildSizeProperty() {
        return totalGuildSize;
    }

    public StringProperty totalUserSizeProperty() {
        return totalUserSize;
    }



}
