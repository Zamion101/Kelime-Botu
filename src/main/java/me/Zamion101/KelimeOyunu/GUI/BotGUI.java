package me.Zamion101.KelimeOyunu.GUI;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import me.Zamion101.KelimeOyunu.BotUtils;
import me.Zamion101.KelimeOyunu.GUI.utils.GuildData;
import me.Zamion101.KelimeOyunu.GUI.utils.TextAreaOutputStream;
import me.Zamion101.KelimeOyunu.KelimeBot;
import me.Zamion101.KelimeOyunu.experimentals.commands.UptimeCommand;
import me.Zamion101.KelimeOyunu.experimentals.utils.GuildMemory;
import sx.blah.discord.handle.obj.IGuild;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.*;

public class BotGUI extends Application {

    static BotGUI instance;
    static FXMLLoader loader;
    static TableView guildTable;

    @Override
    public void start(Stage primaryStage) throws Exception {
        instance = this;
        primaryStage.setTitle("Kelime Botu Kontrol Paneli");
        Pane newLoadedPane = null;
        loader = new FXMLLoader();
        try {
            newLoadedPane = loader.load(getClass().getResource("/Pane.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        primaryStage.setScene(new Scene(newLoadedPane));
        primaryStage.show();
        primaryStage.setResizable(false);
        TextArea textArea = (TextArea) primaryStage.getScene().lookup("#consoleArea");
        textArea.setEditable(false);

        TextAreaOutputStream taos = new TextAreaOutputStream(textArea,500);


        Button clearBtn = (Button) primaryStage.getScene().lookup("#temizleBtn");
        clearBtn.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent event) {
                taos.clear();
            }
        });

        guildTable = (TableView) primaryStage.getScene().lookup("#guildDataTable");
        TextArea errorConsole = (TextArea) primaryStage.getScene().lookup("#errorConsole");

        TextAreaOutputStream errTaos = new TextAreaOutputStream(errorConsole);


        Button clearErrBtn = (Button) primaryStage.getScene().lookup("#temizleErrBtn");
        clearErrBtn.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent event) {
                errTaos.clear();
            }
        });


        Button guildReloadBtn = (Button) primaryStage.getScene().lookup("#greloadBtn");
        guildReloadBtn.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
            @Override
            public void handle(javafx.scene.input.MouseEvent event) {
                BotGUI.initTable();
                Label totalGuild = (Label) primaryStage.getScene().lookup("#totalGuildLabel");
                Label totalUser = (Label) primaryStage.getScene().lookup("#totalUserLabel");

                totalGuild.setText(String.valueOf(BotUtils.getGuildSize()));
                totalUser.setText(String.valueOf(BotUtils.getTotalUser()));
            }
        });


        PrintStream con=new PrintStream(taos);
        PrintStream errCon = new PrintStream(errTaos);
        System.setOut(con);
        System.setErr(errCon);


        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                //Buraya save ekleyebilirim
                System.exit(200);
            }
        });


        new KelimeBot();

        Label uptime = (Label) primaryStage.getScene().lookup("#uptimeLabel");

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        long[] time = UptimeCommand.getTimeDiff(new Date(),new Date(KelimeBot.startTime));
                        uptime.setText(time[0] + " Gün " + time[1] + " Saat " + time[2] + " Dakika " + time[3] + " Saniye");
                    }
                });}
        }, 0, 1000);
    }

    public static void initTable(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                totalGuid = new SimpleStringProperty(String.valueOf(BotUtils.getGuildSize()));
                totalUser = new SimpleStringProperty(String.valueOf(BotUtils.getTotalUser()));
                guildTable.getColumns().remove(0);guildTable.getColumns().remove(0);

                TableColumn<GuildData, String> nameCol = new TableColumn<GuildData, String>("Sunucu Adı");
                TableColumn<GuildData, String> sizeCol = new TableColumn<GuildData, String>("Üye Sayısı");

                nameCol.setCellValueFactory(new PropertyValueFactory<>("guildName"));
                sizeCol.setCellValueFactory(new PropertyValueFactory<>("guildSize"));
                sizeCol.setSortable(false);

                guildTable.setItems(FXCollections.observableList(data));
                guildTable.getColumns().addAll(nameCol,sizeCol);
                guildTable.refresh();
            }
        });
    }

    private static List<GuildData> data = new ArrayList<>();
    public static void addGuild(IGuild guild){
        data.add(new GuildData(new String(guild.getName().getBytes(), Charset.forName("UTF-8")),String.valueOf(guild.getUsers().size())));
        GuildMemory.addGuild(guild);
    }


    public static StringProperty totalUser;
    public static StringProperty totalGuid;

}
